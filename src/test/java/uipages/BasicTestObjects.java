package uipages;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;

public class BasicTestObjects {
	protected static WebDriver driver;

	public WebDriver getDriver() {
		return driver;
	}

	public static void browserSetup(String Url) throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(Url);
		

	}

	public void closeSession() {
		driver.quit();
	}

	public void takeAScreenShot(String FileName) throws IOException {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("C:\\ScreenShots\\" + FileName));

	}

	public static boolean IsElementPresent(String Xpath) {
		if (driver.findElements(By.xpath(Xpath)).size() != 0) {
			System.out.println("ELEMENT IS PRESENT");
			return true;
		} else {
			System.out.println("ELEMENT IS ABSENT");
			return false;
		}

	}
}

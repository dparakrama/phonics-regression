package uipages;

import java.util.Date;

import org.openqa.selenium.WebDriver;

public class ClassPage extends BasicPageObjects {
	String btnClass = "/html/body/app-root/app-core-layout/app-header/header/app-navbar[1]/nav/ul/li[3]/a/span[1]";
	String btnCreateClass = "/html/body/app-root/app-core-layout/main/div/app-class-room/app-core-layout-footer/div/div/div/div/div[2]/div/app-button/button";
	String txtClassroomName = "//*[@id=\'classroomName\']";
	String btnYearLevel = "//*[@id=\'yearLevelId\']";
	String btnYearLevelOption = "//*[@id=\'yearLevelId\']/option[2]";
	String btnTeacher = "//*[@id=\'teacher\']";
	String btnTeacherOption = "//*[@id=\"teacher\"]/option[3]";
	String btnCreateClassModel = "//*[@id=\'createClassModal\']/div/div[1]/div/div[3]/div/app-button/button";
	String btnClose = "//*[@id='createClassModal']/div/div[1]/div/div[1]/div/button";

	public ClassPage(WebDriver driver) {
		super(driver);

	}

	public void navigatetoClassPage() throws InterruptedException {
		clickButton(btnClass);
	}

	public void createNewClass() throws InterruptedException {
		clickButton(btnCreateClass);

	}

	public void provideClassDetails() throws InterruptedException {
		Date date = new Date();
		
		enterText(txtClassroomName, "AutomatedClass" + date);
		clickButton(btnYearLevel);
		clickButton(btnYearLevelOption);
		Thread.sleep(4000);
		clickButton(btnClose);
		clickButton(btnCreateClass);
		
		//moveAndClick(btnTeacher);
		//moveAndClick(btnTeacherOption);
		Thread.sleep(4000);

	}

	public void createClass() throws InterruptedException {
		clickButton(btnCreateClassModel);
		Thread.sleep(4000);

	}

}

package uipages;

import org.openqa.selenium.WebDriver;

public class LoginPage extends BasicPageObjects{
	String txtEmail = "//*[@id=\'userName\']";
	String btnNext = "//*[contains(text(),' Next ')]";
	String txtPassowrd = "//*[@id=\'password\']";
	
	String textWelcomeMessage = "//*[contains(text(),' Welcome ')]";

	public LoginPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	
	public void enteremail(String email) throws InterruptedException {
		enterText(txtEmail,email);
		clickButton(btnNext);
		
	}

	
	public void enterPassword(String password) throws InterruptedException {
		enterText(txtPassowrd,password);
		clickButton(btnNext);
		
		
	}
	
	public void verifyHomePage () throws InterruptedException {
		getContent(textWelcomeMessage,"Welcome");
	}
	
	
	
}

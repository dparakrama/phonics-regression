package uipages;

import java.util.Date;

import org.openqa.selenium.WebDriver;

public class StudentPage extends BasicPageObjects {
	String btnStudent = "/html/body/app-root/app-core-layout/app-header/header/app-navbar[1]/nav/ul/li[2]/a";
	String btnCreateStudent = "/html/body/app-root/app-core-layout/main/div/app-students/app-core-layout-footer/div/div/div/div/div[2]/div/app-button/button";
	String txtGivenName = "//*[@id=\'firstName\']";
	String txtSurname = "//*[@id=\'lastName\']";
	String txtID = "//*[@id=\'studentId\']";
	String btnCreateStudentModel = "//*[@id=\'addStudentModal\']/div/div[1]/div/div[3]/div/app-button/button";

	public StudentPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void navigatetoStudentPage() throws InterruptedException {
		clickButton(btnStudent);
	}

	public void createNewStudent() throws InterruptedException {
		clickButton(btnCreateStudent);

	}

	public void provideStudentDetails() throws InterruptedException {
		Date date = new Date();
		//click("firstName");
		enterText(txtGivenName, "FirstName" + date);
		enterText(txtSurname, "LastName" + date);
		enterText(txtID, "ID" + date);

	}

	public void createStudent() throws InterruptedException {
		clickButton(btnCreateStudentModel);
		Thread.sleep(4000);

	}

}
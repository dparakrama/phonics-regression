package uipages;

import java.util.Date;

import org.openqa.selenium.WebDriver;

public class PhonicsCheckPage extends BasicPageObjects {
	String btnPhonicsCheck = "/html/body/app-root/app-core-layout/app-header/header/app-navbar[1]/nav/ul/li[4]/a";
	String btnSetupPhonicsCheck = "/html/body/app-root/app-core-layout/main/div/app-assessments/app-core-layout-footer/div/div/div/div/div[2]/div/app-button/button";
	String btnClass = "/html/body/app-root/app-core-layout/main/div/app-create-assessment/form/div/div[3]/div/app-form-section[1]/div/div[2]/div[2]/div[1]/app-class-card/div/div[1]";	
	String drpPhonicsCheck = "//*[@id=\'testId\']";
	String drpOption1 = "//*[@id=\'testId\']/option[2]";
	String txtName = "//*[@id=\'assessmentName\']";
	String btnCreate = "/html/body/app-root/app-core-layout/main/div/app-create-assessment/form/app-core-layout-footer/div/div/div/div/div[2]/div/app-button/button";

	public PhonicsCheckPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	

	

	public void navigatetoPhonicsCheck() throws InterruptedException {
		clickButton(btnPhonicsCheck);
	}

	public void setupPhonicsCheck() throws InterruptedException {
		clickButton(btnSetupPhonicsCheck);

	}

	public void providePhonicsCheckDetails() throws InterruptedException {
		Date date = new Date();
		clickButton(btnClass);
		clickButton(drpPhonicsCheck);
		clickButton(drpOption1);
		enterText(txtName, "PhonicsCheck" + date);
		

	}

	public void createPhonicsCheck() throws InterruptedException {
		clickButton(btnCreate);
		Thread.sleep(4000);

	}

}


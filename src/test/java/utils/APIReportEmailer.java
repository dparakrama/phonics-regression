package utils;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.junit.Test;




public class APIReportEmailer {
	String filename1 = "C:\\Users\\dparakrama\\OneDrive - Education Services Australia LTD\\Desktop\\cucumber.jpg";

	
	@Test
	public void sendAMailPVTMyScis() {


		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(
								"esatestteam2@gmail.com", "P3ar$onDP1");

					}

				});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("esatestteam2@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse("esatestteam2@gmail.com"));
			message.setSubject("SCIS PVT REPORT MYSCIS | PROD");
			BodyPart messageBodyPart1 = new MimeBodyPart();
			messageBodyPart1.setText("PLESE FIND THE ATTACHMENTS");
			Multipart multipart = new MimeMultipart();

			addAttachment(multipart, filename1);
		
			
			message.setContent(multipart);
			Transport.send(message);
			System.out.println("=====Email Sent=====");
			
			FileDeleter.fileDeleter(filename1);
			
		} catch (MessagingException e) {
			throw new RuntimeException(e);

		}

	}
	
	
	private static void addAttachment(Multipart multipart, String filename)
			throws MessagingException {
		
		DataSource source = new FileDataSource(filename);
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(filename);
		multipart.addBodyPart(messageBodyPart);
	}
	
	
	
}

Feature: Test GET requests based on System Admin user

  Background: 
    * url 'https://sit-sc-phonicshub.esa.edu.au'
    * header Content-Type = 'application/json'
     * def user =
      """
      {
      "userName": "applicationsupport@esa.edu.au",
      "password": "Qazwsxedc12345#"
      }
      """

  Scenario: Login as Sys Admin and test GET requests SSO and email
   
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    And match header X-Content-Type-Options contains 'nosniff'
    And match header X-Frame-Options contains 'SAMEORIGIN'
    And match header X-XSS-Protection contains '1; mode=block'
    And match header Content-Security-Policy contains "default-src 'self' www.googletagmanager.com www.google-analytics.com static.cloudflareinsights.com fonts.gstatic.com *.fontawesome.com fonts.googleapis.com 'unsafe-inline' data:"
    * def accessToken = response.token
   
    Given path 'api/v1/General/States/'
    And header Authorization = 'Bearer ' + accessToken
    When method GET
    Then status 200
    #And print 'response---', response
     And match header X-Content-Type-Options contains 'nosniff'
    And match header X-Frame-Options contains 'SAMEORIGIN'
    And match header X-XSS-Protection contains '1; mode=block'
    And match header Content-Security-Policy contains "default-src 'self' www.googletagmanager.com www.google-analytics.com static.cloudflareinsights.com fonts.gstatic.com *.fontawesome.com fonts.googleapis.com 'unsafe-inline' data:"
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/General/Sectors/'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/General/YearLevel/'
    When request {withRoles:='true'}
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/General/DeliveryModes/'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/General/Scopes/'
    When request {withRoles:='true'}
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Scenario: Login as Sys Admin and test GET requests Permission Role
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    * def accessToken = response.token
    
    #2d01d043-759a-4968-b4c5-54495336b758
    #3e583f42-1f09-4614-97da-63b95a3ec8f3
    # Permission/Role
    Given path 'api/v1/Permission/Role/'
    And header Authorization = 'Bearer ' + accessToken
    When request {roleId:='2d01d043-759a-4968-b4c5-54495336b758'}
    Then method GET
    And status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    #ROLE
    Given path 'api/v1/Roles'
    When method GET
    Then status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
        
    # USER
    Scenario: Login as Sys Admin and test GET requests User
     Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    * def accessToken = response.token
    
    Given path 'api/v1/User/98cdb66f-f6b3-4a46-a33a-fba15dc66072'
    And header Authorization = 'Bearer ' + accessToken
    When method GET
    Then status 204
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/User/UserScope/'
    And header Authorization = 'Bearer ' + accessToken
    When request {userId:='98cdb66f-f6b3-4a46-a33a-fba15dc66072'}
    Then method GET
    And status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'v1/Notifications/GetNotificationsForUserById'
    And header Authorization = 'Bearer ' + accessToken
    When request {userId:='98cdb66f-f6b3-4a46-a33a-fba15dc66072'}
    Then method GET
    Then status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Scenario: Login as Sys Admin and test GET requests State and Sector
     Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    * def accessToken = response.token
    
    Given path 'api/v1/States/'
    And header Authorization = 'Bearer ' + accessToken
    When method GET
    Then status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Sectors/'
    When method GET
    Then status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
     # GENERAL
    Scenario: Login as Sys Admin and test GET requests General
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    * def accessToken = response.token
    
     # EMAIL
    Given path 'api/v1/EmailDomainWhiteLists/'
    And header Authorization = 'Bearer ' + accessToken
    When method GET
    Then status 200
    And print 'response---', response
    And match header X-Content-Type-Options contains 'nosniff'
    And match header X-Frame-Options contains 'SAMEORIGIN'
    And match header X-XSS-Protection contains '1; mode=block'
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/EmailDomainWhiteLists/'
    When request {id:='1'}
    Then method GET
    And status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
     Given path 'api/v1/EmailDomainWhiteLists/Check'
    When request {sector:='1', state:='1', email:='esatestteam2@gmail.com'}
    Then method GET
    And status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
   
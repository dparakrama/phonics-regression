Feature: Test POST requests based on School Admin user

  Background: 
    * url 'https://sit-sc-phonicshub.esa.edu.au'
    * header Content-Type = 'application/json'
    * def user =
      """
      {
      "userName": "esatestteam2@gmail.com",
      "password": "Passw0rd12345!"
      }
      """
  Scenario: Login as School Admin and test POST requests
      
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    And match header X-Content-Type-Options contains 'nosniff'
    And match header X-Frame-Options contains 'SAMEORIGIN'
    And match header X-XSS-Protection contains '1; mode=block'
    And match header Content-Security-Policy contains "default-src 'self' www.googletagmanager.com www.google-analytics.com static.cloudflareinsights.com fonts.gstatic.com *.fontawesome.com fonts.googleapis.com 'unsafe-inline' data:"
    * def accessToken = response.token
    
    #CLASSROOM
    Given path 'api/v1/Classrooms/Search/'
    And header Authorization = 'Bearer ' + accessToken
    When request {id:'152', classroomName:'seltest2'}
    Then method POST
    And status 200
    #And print 'response---', response
    And match header X-Content-Type-Options contains 'nosniff'
    And match header X-Frame-Options contains 'SAMEORIGIN'
    And match header X-XSS-Protection contains '1; mode=block'
    And match header Content-Security-Policy contains "default-src 'self' www.googletagmanager.com www.google-analytics.com static.cloudflareinsights.com fonts.gstatic.com *.fontawesome.com fonts.googleapis.com 'unsafe-inline' data:"
    * header Authorization = 'Bearer ' + accessToken
    
    Scenario: Login as School Admin and test POST requests Assessment
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    #And print 'response---', response
    * def accessToken = response.token
    
    #Assessment
    Given path 'api/v1/Assessment/GetAssessmentResults'
    And header Authorization = 'Bearer ' + accessToken
    When request {studentId:'4',  teacherId:'5c9bc735-0237-476e-8b81-9d346837d7a2', pageIndex:'1',pageSize:'40'}
    Then method POST
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Scenario: Login as School Admin and test POST requests Classroom Student
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    #And print 'response---', response
    * def accessToken = response.token
    
    #STUDENTS
    #api/v1/Students/Search
    Given path 'api/v1/Students/Search/'
    And header Authorization = 'Bearer ' + accessToken
    When request {id: '2', firstName:'jess', lastName:'student 1'}
    Then method POST
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Scenario: Login as School Admin and test POST requests Classroom Report
    #CLASSROOMSTUDENT
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    #And print 'response---', response
    * def accessToken = response.token
    
    Given path 'api/v1/ClassroomStudents/SearchAssociatedStudents/'
    And header Authorization = 'Bearer ' + accessToken
    When request {classroomName:'Jess Class', pageIndex:'1', pageSize:'5', sortBy:'classroomName',sortAsc:'true'}
    Then method POST
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/ClassroomStudents/GetAssociatedStudentsByClass/'
    #And header Authorization = 'Bearer ' + accessToken
    When request {classroomName:'Jess Class', pageIndex:'1', pageSize:'5', sortBy:'classroomName',sortAsc:'true'}
    Then method POST
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Scenario: Login as School Admin and test POST requests Student Report
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    #And print 'response---', response
    * def accessToken = response.token
    
    #SearchStudentReports
    Given path 'api/v1/Reports/SearchStudentReports/'
    And header Authorization = 'Bearer ' + accessToken
    When request {studentNameOrId: '2', teacherId: 'aa1ffbdf-c4da-432f-9765-10ac43281ec7', classroomId: '2', assessmentId: '2', pageIndex:'1', pageSize:'5', sortBy:'studentNameOrId',sortAsc:'true'}
    Then method POST
    And status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Assessment/StudentReportSearch/Excel'
    And header Authorization = 'Bearer ' + accessToken
    When request {studentId:'2',  teacherId:'aa1ffbdf-c4da-432f-9765-10ac43281ec7', classroomId: '2', assessmentId: '1'}
    Then method POST
    And status 200
    And print 'response---', response
    
    Given path 'api/v1/Assessment/StudentReportSearchByWord/Excel'
    And header Authorization = 'Bearer ' + accessToken
    When request {studentId:'2',  teacherId:'aa1ffbdf-c4da-432f-9765-10ac43281ec7', classroomId: '2', assessmentId: '1'}
    Then method POST
    And status 200
    And print 'response---', response
    
   
    
   
    

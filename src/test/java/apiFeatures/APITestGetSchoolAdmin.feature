Feature: Test GET requests based on School Admin user

  Background: 
    * url 'https://sit-sc-phonicshub.esa.edu.au'
    * header Content-Type = 'application/json'
     * def user =
      """
      {
      "userName": "esatestteam2@gmail.com",
      "password": "Passw0rd12345!"
      }
      """

  Scenario: Login as School Admin and test GET requests Classrooms
   
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    And match header X-Content-Type-Options contains 'nosniff'
    And match header X-Frame-Options contains 'SAMEORIGIN'
    And match header X-XSS-Protection contains '1; mode=block'
    And match header Content-Security-Policy contains "default-src 'self' www.googletagmanager.com www.google-analytics.com static.cloudflareinsights.com fonts.gstatic.com *.fontawesome.com fonts.googleapis.com 'unsafe-inline' data:"
    * def accessToken = response.token
    
    #CLASSROOM
		Given path 'api/v1/Classrooms/1'
    And header Authorization = 'Bearer ' + accessToken
    Then method GET
    And status 200
    And match header X-Content-Type-Options contains 'nosniff'
    And match header X-Frame-Options contains 'SAMEORIGIN'
    And match header X-XSS-Protection contains '1; mode=block'
    And match header Content-Security-Policy contains "default-src 'self' www.googletagmanager.com www.google-analytics.com static.cloudflareinsights.com fonts.gstatic.com *.fontawesome.com fonts.googleapis.com 'unsafe-inline' data:"
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
		Given path 'api/v1/Classrooms/List'
    And request {schoolId:='6191'}
    When method GET
    Then status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Scenario: Login as School Admin and test GET requests for Student
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    * def accessToken = response.token
    
  	#STUDENT
	   Given path 'api/v1/Students/1'
	   And header Authorization = 'Bearer ' + accessToken
	   When method GET
	   Then status 200
	   And print 'response---', response
	   * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Students/List?schoolId=1&includeArchive=true'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
		
		Scenario: Login as School Admin and test GET requests for Assessment
		Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    * def accessToken = response.token
    
		Given path 'api/v1/Assessment'
		And header Authorization = 'Bearer ' + accessToken
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Assessment/1'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Assessment/Test'
		And request {testId:='1'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
		
		Given path 'api/v1/Assessment/GetSuggestedResources'
		And request {studentId:='2'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Assessment/GetAssessmentDashboardData'
		And request {teacherId:='eb53b387-c366-49a7-b93b-3965d80ed69d'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    #can only access by teacher, permission feature 'api/v1/Assessment/StartAssessment' , 
    #no student code on current data??? can only access by teacher, permission feature  'api/v1/Assessment/Results/Detailed'
    Given path 'api/v1/Assessment/Class'
		And request {classId:='2'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Assessment/Student'
		And request {studentId:='2'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
 
		Given path 'api/v1/Assessment/GenerateQRCode'
		And request {studentCode:='JSPMD3135'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    #TODO
    #https://sit-sc-phonicshub.esa.edu.au/api/v1/Assessment/Preference?preferenceType=testtype&assessmentId=1
    
    Scenario: Login as School Admin and test GET requests for General
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    * def accessToken = response.token
    
    #GENERAL
    Given path 'api/v1/General/States/'
    And header Authorization = 'Bearer ' + accessToken
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/General/Sectors/'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/General/YearLevel/'
    When request {withRoles:='true'}
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/General/DeliveryModes/'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/General/Scopes/'
    When request {withRoles:='true'}
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Scenario: Login as School Admin and test GET requests for Permission/Role
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    * def accessToken = response.token
    
    # Permission/Role
    Given path 'api/v1/Permission/Role/'
    And header Authorization = 'Bearer ' + accessToken
    When request {roleId:='3e583f42-1f09-4614-97da-63b95a3ec8f3'}
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    #ROLE
    Given path 'api/v1/Roles'
    When method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
     
    Scenario: Login as School Admin and test GET requests for User
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    * def accessToken = response.token
    
    # USER
    Given path 'api/v1/User/98cdb66f-f6b3-4a46-a33a-fba15dc66072'
    And header Authorization = 'Bearer ' + accessToken
    When method GET
    Then status 204
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/User/UserScope/'
    When request {userId:='98cdb66f-f6b3-4a46-a33a-fba15dc66072'}
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
		
		Scenario: Login as School Admin and test GET requests for Test
		Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    * def accessToken = response.token
    
		#TEST
		Given path 'api/v1/Test'
		And header Authorization = 'Bearer ' + accessToken
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Test/1'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
		
		Scenario: Login as School Admin and test GET requests for School
		Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    * def accessToken = response.token
    
		#SCHOOLS
		Given path 'api/v1/Schools/List'
		And header Authorization = 'Bearer ' + accessToken
    When request {stateId:='1', sectorId:='3'}
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
		
		#https://sit-sc-phonicshub.esa.edu.au/api/v1/Schools/1
		Given path 'api/v1/Schools/2'
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
		
		Given path 'api/v1/Schools/GetTeachersBySchool'
    When request {schoolId:='2'}
    Then method GET
    And status 204
    #And print 'response---', response
    #* header Authorization = 'Bearer ' + accessToken
    
    Scenario: Login as School Admin and test GET requests for  Report
		Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    * def accessToken = response.token
    
    Given path 'api/v1/Reports/StudentsResultsCategories'
		And header Authorization = 'Bearer ' + accessToken
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Reports/GetGraphDataForStudentAssessment'
		And header Authorization = 'Bearer ' + accessToken
		When request {studentId:='2', assessmentId:='2'}
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Reports/GetWordsByCategoriesForAssessment'
		And header Authorization = 'Bearer ' + accessToken
		When request {assessmentId:='2'}
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken

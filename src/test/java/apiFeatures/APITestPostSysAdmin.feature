Feature: Test POST requests based on System Admin user

  Background: 
    * url 'https://sit-sc-phonicshub.esa.edu.au'
    * header Content-Type = 'application/json'
     * def user =
      """
      {
       "userName": "applicationsupport@esa.edu.au",
      "password": "Qazwsxedc12345#"
      }
      """

  Scenario: Login as School Admin and test POST requests
   
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
     And match header X-Content-Type-Options contains 'nosniff'
    And match header X-Frame-Options contains 'SAMEORIGIN'
    And match header X-XSS-Protection contains '1; mode=block'
    And match header Content-Security-Policy contains "default-src 'self' www.googletagmanager.com www.google-analytics.com static.cloudflareinsights.com fonts.gstatic.com *.fontawesome.com fonts.googleapis.com 'unsafe-inline' data:"
    * def accessToken = response.token
    
    
    #api/v1/Roles/Search
    Given path 'api/v1/Roles/Search/'
    And header Authorization = 'Bearer ' + accessToken
    When request {name:'Teacher'}
    Then method POST
    And status 200
    And print 'response---', response
    And match header X-Content-Type-Options contains 'nosniff'
    And match header X-Frame-Options contains 'SAMEORIGIN'
    And match header X-XSS-Protection contains '1; mode=block'
    And match header Content-Security-Policy contains "default-src 'self' www.googletagmanager.com www.google-analytics.com static.cloudflareinsights.com fonts.gstatic.com *.fontawesome.com fonts.googleapis.com 'unsafe-inline' data:"
    * header Authorization = 'Bearer ' + accessToken
    
    Scenario: Login as School Admin and test POST requests
   
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    * def accessToken = response.token
    
    #User
    Given path 'api/v1/User'
    And header Authorization = 'Bearer ' + accessToken
    When request {schoolId:'1',  pageIndex: '1', pageSize: '5'}
    Then method POST
    And status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/User/Logout'
    And header Authorization = 'Bearer ' + accessToken
    When request {userId:'4b541a21-f454-4bd5-9ebb-6a78ba48b96f'}
    Then method POST
    And status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    
     #api/v1/EmailDomainWhiteLists/Search
    #EMAIL
    Given path 'api/v1/EmailDomainWhiteLists/Search/'
    And header Authorization = 'Bearer ' + accessToken
    When request {stateName:'Victoria', sectorName:'Independent', isActive:'true', domainWhitelist:'gmail.com'}
    Then method POST
    And status 200
    And print 'response---', response
    And match header X-Content-Type-Options contains 'nosniff'
    And match header X-Frame-Options contains 'SAMEORIGIN'
    And match header X-XSS-Protection contains '1; mode=block'
    * header Authorization = 'Bearer ' + accessToken
   
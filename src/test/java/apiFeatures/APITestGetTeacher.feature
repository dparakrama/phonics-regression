Feature: Test GET requests based on Teacher user

  Background: 
    * url 'https://sit-sc-phonicshub.esa.edu.au'
    * header Content-Type = 'application/json'
    
    * def user =
      """
      {
      "userName": "esatestteam201@gmail.com",
      "password": "Passw0rd12345!"
      }
      """

  Scenario: Login as Teacher and test GET requests
    
   Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    And match header X-Content-Type-Options contains 'nosniff'
    And match header X-Frame-Options contains 'SAMEORIGIN'
    And match header X-XSS-Protection contains '1; mode=block'
    And match header Content-Security-Policy contains "default-src 'self' www.googletagmanager.com www.google-analytics.com static.cloudflareinsights.com fonts.gstatic.com *.fontawesome.com fonts.googleapis.com 'unsafe-inline' data:"
    * def accessToken = response.token
    
    #CLASSROOM
    Given path 'api/v1/Classrooms/3'
    And header Authorization = 'Bearer ' + accessToken
    When method GET
    Then status 200
    And print 'response---', response
    And match header X-Content-Type-Options contains 'nosniff'
    And match header X-Frame-Options contains 'SAMEORIGIN'
    And match header X-XSS-Protection contains '1; mode=block'
    And match header Content-Security-Policy contains "default-src 'self' www.googletagmanager.com www.google-analytics.com static.cloudflareinsights.com fonts.gstatic.com *.fontawesome.com fonts.googleapis.com 'unsafe-inline' data:"
    
    Given path 'api/v1/Classrooms/List'
    And request {schoolId:='6191'}
    When method GET
    Then status 200
    And print 'response---', response
    
   Scenario: Login as Teacher and test GET requests for Student
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    * def accessToken = response.token
    
   #STUDENT
   Given path 'api/v1/Students/7'
   And header Authorization = 'Bearer ' + accessToken
   When method GET
   Then status 200
    #And print 'response---', response
    
    Given path 'api/v1/Students/List?schoolId=1&includeArchive=true'
    And request {schoolId:='6191',includeArchive:='true'}
    When method GET
    Then status 200
    #And print 'response---', response
    
    Scenario: Login as Teacher and test GET requests for Assessment
    
    Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    * def accessToken = response.token
     
		#ASSESSMENT
    Given path 'api/v1/Assessment/AssessmentStatuses'
    And header Authorization = 'Bearer ' + accessToken
    When method GET
    Then status 200
    And print 'response---', response
    
    Given path 'api/v1/Assessment'
    When method GET
    Then status 200
    And print 'response---', response
  * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Assessment/1'
    When method GET
    Then status 200
    And print 'response---', response
   * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Assessment/Test'
		And request {testId:='1'}
    When method GET
    Then status 200
    And print 'response---', response
   * header Authorization = 'Bearer ' + accessToken
    
    #all assessment list
		Given path 'api/v1/Assessment'
    When method GET
    Then status 200
    #And print 'response---', response
   * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Assessment/Test'
		And request {testId:='1'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
		
		Given path 'api/v1/Assessment/GetSuggestedResources'
		And request {studentId:='7'}
    When method GET
    Then status 200
    #And print 'response---', response
   * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Assessment/GetAssessmentDashboardData'
		And request {teacherId:='f385a693-8b2b-4477-b2f8-12439724b6b8'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
   
    #can only access by teacher, permission feature 'api/v1/Assessment/StartAssessment' , 
    #no student code on current data??? can only access by teacher, permission feature  'api/v1/Assessment/Results/Detailed'
    Given path 'api/v1/Assessment/Class'
		And request {classId:='2'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Assessment/Student'
		And request {studentId:='7'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
		Given path 'api/v1/Assessment/GenerateQRCode'
		And request {assessmentId:='1', studentCode:='JSYIH97'}
    When method GET
    Then status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    # * header Content-Type = 'application/json'
    Given path 'api/v1/Assessment/Results/Detailed'
		And request {studentCode:='JSYIH97'}
		And header Content-Type = 'text/plain'
    When method GET
    Then status 200
    And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Assessment/StartAssessment'
    And header Authorization = 'Bearer ' + accessToken
    And request {assessmentId:='1'}
    And header Content-Type = 'text/plain'
    When method GET
    Then status 200
    And print 'response---', response
   * header Authorization = 'Bearer ' + accessToken
    
    
    
    Scenario: Login as School Admin and test GET requests for  Report
		Given path 'api/v1/Auth/login'
    And request user
    When method POST
    Then status 200
    And print 'response---', response
    * def accessToken = response.token
    
    Given path 'api/v1/Reports/StudentsResultsCategories'
		And header Authorization = 'Bearer ' + accessToken
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Reports/GetGraphDataForStudentAssessment'
		And header Authorization = 'Bearer ' + accessToken
		When request {studentId:='7', assessmentId:='1'}
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
    
    Given path 'api/v1/Reports/GetWordsByCategoriesForAssessment'
		#And header Authorization = 'Bearer ' + accessToken
		When request {assessmentId:=1}
    Then method GET
    And status 200
    #And print 'response---', response
    * header Authorization = 'Bearer ' + accessToken
     
   
    
  

    
   
package uiTestRunner;

import org.junit.runner.RunWith;		
import cucumber.api.CucumberOptions;		
import cucumber.api.junit.Cucumber;		

@RunWith(Cucumber.class)			
@CucumberOptions(features="classpath:uiFeatures",glue={"uistepdefinition"},
plugin = { "pretty", "json:target/cucumber-ui.json" },
monochrome = true)
public class Runner 				
{		

}
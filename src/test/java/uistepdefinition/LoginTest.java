package uistepdefinition;

import uipages.BasicTestObjects;
import uipages.LoginPage;
import utils.XmlReader;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginTest extends BasicTestObjects {

	@Given("^Open the Chrome and launch the application$")
	public void open_the_Chrome_and_launch_the_application() throws Throwable {
		String url = XmlReader.readXml("url", "sit");
		browserSetup(url);
	}

	@When("^the email and password is entered and click login$")
	public void the_email_and_password_is_entered_and_click_login() throws Throwable {
		String email = XmlReader.readXml("SchoolAdmin", "email");
		LoginPage LoginPage = new LoginPage(driver);
		LoginPage.enteremail(email);
		String password = XmlReader.readXml("SchoolAdmin", "password");
		LoginPage.enterPassword(password);
		
		
	}

	@Then("^User should be logged in successfully to home page$")
	public void user_should_be_logged_in_successfully_to_home_page() throws Throwable {
		LoginPage LoginPage = new LoginPage(driver);
		LoginPage.verifyHomePage();
		driver.close();
	
		
	}



}
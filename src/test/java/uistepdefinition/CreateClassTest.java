package uistepdefinition;

import uipages.BasicTestObjects;
import uipages.ClassPage;
import uipages.LoginPage;
import utils.XmlReader;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
public class CreateClassTest extends BasicTestObjects {
	
	
	
	@Given("^A user navigate to home page$")
	public void a_user_navigate_to_home_page() throws Throwable {
		String url = XmlReader.readXml("url", "sit");
		browserSetup(url);
		String email = XmlReader.readXml("SchoolAdmin", "email");
		LoginPage LoginPage = new LoginPage(driver);
		LoginPage.enteremail(email);
		String password = XmlReader.readXml("SchoolAdmin", "password");
		LoginPage.enterPassword(password);

	    
	   
	}

	@Given("^click class page$")
	public void click_class_page() throws Throwable {
		ClassPage ClassPage = new ClassPage(driver);
		ClassPage.navigatetoClassPage();
	  
	}

	@When("^The user click create class button$")
	public void the_user_clickc_create_class_button() throws Throwable {
		ClassPage ClassPage = new ClassPage(driver);
		ClassPage.createNewClass();
	   
	}
	
	@When("^provide details$")
	public void provide_details() throws Throwable {
		ClassPage ClassPage = new ClassPage(driver);
		ClassPage.provideClassDetails();
	}

	@When("^click create class button$")
	public void click_create_class_button() throws Throwable {
		ClassPage ClassPage = new ClassPage(driver);
		ClassPage.createClass();
	    
	}

	@Then("^The user is able to see the crated class$")
	public void the_user_is_able_to_see_the_crated_class() throws Throwable {
		driver.close();
	    throw new PendingException();
	}

}

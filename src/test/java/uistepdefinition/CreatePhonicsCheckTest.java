package uistepdefinition;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import uipages.BasicTestObjects;
import uipages.LoginPage;
import uipages.PhonicsCheckPage;
import utils.XmlReader;

public class CreatePhonicsCheckTest extends BasicTestObjects {

	@Given("^user login and navigate to home page$")
	public void user_login_and_navigate_to_home_page() throws Throwable {
		String url = XmlReader.readXml("url", "sit");
		browserSetup(url);
		String email = XmlReader.readXml("SchoolAdmin", "email");
		LoginPage LoginPage = new LoginPage(driver);
		LoginPage.enteremail(email);
		String password = XmlReader.readXml("SchoolAdmin", "password");
		LoginPage.enterPassword(password);

	}

	@Given("^click Phonics Check page$")
	public void click_Phonics_Check_page() throws Throwable {

		PhonicsCheckPage phonicsCheckPage = new PhonicsCheckPage(driver);
		phonicsCheckPage.navigatetoPhonicsCheck();

		throw new PendingException();
	}

	@When("^The user click class and provide details$")
	public void the_user_click_class_and_provide_details() throws Throwable {
		PhonicsCheckPage phonicsCheckPage = new PhonicsCheckPage(driver);
		phonicsCheckPage.setupPhonicsCheck();

		phonicsCheckPage.providePhonicsCheckDetails();

		throw new PendingException();
	}

	@When("^click create phonics check button$")
	public void click_create_phonics_check_button() throws Throwable {
		PhonicsCheckPage phonicsCheckPage = new PhonicsCheckPage(driver);
		phonicsCheckPage.createPhonicsCheck();

		throw new PendingException();
	}

	@Then("^The user is able to see the crated phonics check$")
	public void the_user_is_able_to_see_the_crated_phonics_check() throws Throwable {
		driver.close();
	}

}

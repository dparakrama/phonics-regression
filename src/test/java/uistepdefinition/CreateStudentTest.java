package uistepdefinition;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import uipages.BasicTestObjects;
import uipages.LoginPage;
import uipages.StudentPage;
import utils.XmlReader;


public class CreateStudentTest extends BasicTestObjects {
	@Given("^user navigate to home page$")
	public void user_navigate_to_home_page() throws Throwable {
		String url = XmlReader.readXml("url", "sit");
		browserSetup(url);
		String email = XmlReader.readXml("SchoolAdmin", "email");
		LoginPage LoginPage = new LoginPage(driver);
		LoginPage.enteremail(email);
		String password = XmlReader.readXml("SchoolAdmin", "password");
		LoginPage.enterPassword(password);
	}
	
	
	@Given("^click student page$")
	public void click_student_page() throws Throwable {
		StudentPage StudentPage = new StudentPage(driver);	
		StudentPage.navigatetoStudentPage();
		
	}

	@When("^The user click create student button$")
	public void the_user_click_create_student_button() throws Throwable {
		StudentPage StudentPage = new StudentPage(driver);
		StudentPage.createNewStudent();
	}

	@When("^click create student button$")
	public void click_create_student_button() throws Throwable {
		StudentPage StudentPage = new StudentPage(driver);
		StudentPage.provideStudentDetails();
		StudentPage.createStudent();
	}

	@Then("^The user is able to see the crated student$")
	public void the_user_is_able_to_see_the_crated_student() throws Throwable {
		driver.close();
		throw new PendingException();
	}
}

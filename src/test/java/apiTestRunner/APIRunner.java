package apiTestRunner;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.runner.RunWith;

import com.intuit.karate.junit4.Karate;

import cucumber.api.CucumberOptions;
import utils.CreateZip;
//import utils.XmlReader;

@RunWith(Karate.class)		
@CucumberOptions(features="classpath:apiFeatures",
plugin = { "pretty", "html:target/cucumber-reports/cucumber-html", "json:target/cucumber.json","junit:target/cucumber.xml",
		"rerun:target/rerun.text"}, 
monochrome = true)
public class APIRunner {
	

    @AfterClass
    public static void zippingResults() throws IOException {
    	CreateZip.createZip();
    }
}
